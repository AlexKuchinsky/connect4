package com.example.alex.connect4.GameInstructions

import com.example.alex.connect4.GameInstructions.Models.Chip
import com.example.alex.connect4.GameInstructions.Models.ChipPositionDescription
import com.example.alex.connect4.GameInstructions.Models.Player

class Playground(val columns: Int, val rows: Int, val players: ArrayList<Player>) {
    private val matrix: Array<Array<Chip?>> = Array<Array<Chip?>>(columns,{ i -> Array<Chip?>(rows,{ _ -> null})})

    private var currentPlayerIndex: Int = -1

    private val playersCount: Int
            get() = players.size

    private fun nextPlayer(): Player {
        if(currentPlayerIndex == playersCount - 1 )
            currentPlayerIndex = -1
        return players[++currentPlayerIndex]
    }

    fun setChip(chipPositionDescription: ChipPositionDescription): Chip? {
        val column = chipPositionDescription.column
        if(isColumnAvailable(column)){
            val currentPlayer = nextPlayer()
            val currentPlayerChip = Chip(currentPlayer.playerId)
            val row = getColumnHeight(column)
            matrix[column][row] = currentPlayerChip
            return  currentPlayerChip
        }
        else
            return null

    }

    private fun getColumnHeight(column: Int): Int {
        for( row in 0 until rows)
            if(matrix[column][row] == null)
                return row
        return -1
    }
    private fun isColumnAvailable(column: Int): Boolean{
        return getColumnHeight(column) >= 0
    }
}

