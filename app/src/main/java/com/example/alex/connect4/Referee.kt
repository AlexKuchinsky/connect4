package com.example.alex.connect4

import android.net.IpPrefix
//import android.support.v4.app.INotificationSideChannel
import android.util.Log
import java.io.Serializable
import java.sql.Ref
import java.sql.RowId

class Referee private constructor(): Serializable  {

    companion object {
        private val refereeInstance: Referee = Referee()
        private var isInitialized = false
        @Synchronized
        fun getInstance(): Referee {
            return refereeInstance
        }
        fun initialize(playgroundMatrix: IPlayground,playgroundFragment: IPlayground,
                       firstPlayer: Player, secondPlayer: Player,
                       firstPlayerScoreTextView: PlayerScoreTextView, seconPlayerScoreTextView: PlayerScoreTextView){


            getInstance().playgroundFragment = playgroundFragment
            getInstance().firstPlayerScoreTextView = firstPlayerScoreTextView
            getInstance().secondPlayerScoreTextView = seconPlayerScoreTextView
            if(!isInitialized){
                getInstance().playgroundMatrix = playgroundMatrix
                getInstance().firstPlayer = firstPlayer
                getInstance().secondPlayer = secondPlayer
                getInstance().currentPlayer = firstPlayer
                getInstance().firstPlayerScore = 0
                getInstance().secondPlayerScore = 0
            }
            isInitialized = true
        }
        fun reset(){
            isInitialized = false
        }
    }

    lateinit var playgroundMatrix: IPlayground
    lateinit var playgroundFragment: IPlayground

    lateinit var firstPlayer: Player
    lateinit var secondPlayer: Player
    lateinit var currentPlayer: Player

    lateinit var firstPlayerScoreTextView: PlayerScoreTextView
    lateinit var secondPlayerScoreTextView: PlayerScoreTextView

    var firstPlayerScore = 0
    var secondPlayerScore = 0

    protected fun currentPlayerMadeTurn(): Boolean{
        pickUpNextPlayer()
        val setReadyResult = currentPlayer.setReady()
        if(setReadyResult>=0)
            return registerTouch(setReadyResult)
        return false
    }
    var winnerId = 0
    fun registerTouch(column: Int): Boolean{

        //playgroundFragment.lockPlayground(true)
        val row = playgroundMatrix.getColumnHeight(column)
        if(row>=0){
            playgroundMatrix.updatePlayground(column,row,currentPlayer.playerId)
            playgroundFragment.updatePlayground(column,row,currentPlayer.playerId)
            if(checkWinnerPosition(PlaygroundMatrix.getInstance(),column,row,currentPlayer.playerId)){
                winnerId = currentPlayer.playerId
                countWin(winnerId)
                return true
            }
            return currentPlayerMadeTurn()
        }
        return false
        //playgroundFragment.lockPlayground(false)

    }

    // in
    fun countWin(playerId: Int){
        if(playerId==1)
            firstPlayerScore++
        else
            secondPlayerScore++
        firstPlayerScoreTextView.updateSocre(firstPlayerScore)
        secondPlayerScoreTextView.updateSocre(secondPlayerScore)
    }
    fun pickUpNextPlayer(){
        if(currentPlayer.playerId==secondPlayer.playerId){
            currentPlayer = firstPlayer
            secondPlayerScoreTextView.boldPlayerName(false)
            firstPlayerScoreTextView.boldPlayerName(true)
        }
        else{
            currentPlayer = secondPlayer
            secondPlayerScoreTextView.boldPlayerName(true)
            firstPlayerScoreTextView.boldPlayerName(false)
        }
    }
}

fun clearPlaygrounds(playgrounds: Pair<IPlayground,IPlayground>){
    playgrounds.first.clearPlayground()
    playgrounds.second.clearPlayground()
    // TODO:
    // 1) Reset current player
    // 2) Update scores' textViews

}
fun isDraw(playgroundMatrix: IPlayground): Boolean{
    for(i in 0..playgroundMatrix.columns-1)
        if(playgroundMatrix.isColumnAvailable(i))
            return false
    return true
}

fun synchronizePlaygrounds(primaryPlayground: IPlayground, secondaryPlayground: IPlayground ){
    for(column in 0..primaryPlayground.columns-1)
        for(row in 0..primaryPlayground.rows-1){
            val player = primaryPlayground.getOnPosition(column,row)
            if(player!=0)
                secondaryPlayground.updatePlayground(column,row,player)
        }

    // Then need to update players scores
    // and reset current player
}

fun checkWinnerPosition(playground: IPlayground, column: Int, row: Int, playerId: Int): Boolean{
    var columnNumber = column
    var figuresInRow = 0

    // To left, then to right
    while(columnNumber >= 0 && playground.getOnPosition(columnNumber,row)==playerId)
        columnNumber--
    columnNumber++
    while (columnNumber<playground.columns && playground.getOnPosition(columnNumber,row) == playerId){
        columnNumber++
        figuresInRow++
    }
    if(figuresInRow >= 4)
        return true

    // Go down, then up
    var rowNumber = row
    figuresInRow = 0
    while(rowNumber<playground.rows && playground.getOnPosition(column,rowNumber)==playerId)
        rowNumber++
    rowNumber--
    while (rowNumber>=0 && playground.getOnPosition(column,rowNumber)==playerId){
        rowNumber--
        figuresInRow++
    }
    if(figuresInRow >=4)
        return true


    // Go left-up, then right-down
    rowNumber = row
    columnNumber = column
    figuresInRow = 0

    while (rowNumber>=0 && columnNumber>=0 && playground.getOnPosition(columnNumber,rowNumber)==playerId){
        rowNumber--
        columnNumber--
    }
    rowNumber++
    columnNumber++
    while (rowNumber<playground.rows && columnNumber<playground.columns && playground.getOnPosition(columnNumber,rowNumber)==playerId){
        rowNumber++
        columnNumber++
        figuresInRow++
    }
    if(figuresInRow >=4)
        return true

    // Go right-up then left-down
    rowNumber = row
    columnNumber = column
    figuresInRow = 0

    while (rowNumber>=0 && columnNumber<playground.columns && playground.getOnPosition(columnNumber,rowNumber)==playerId){
        rowNumber--
        columnNumber++
    }
    rowNumber++
    columnNumber--
    while (rowNumber<playground.rows && columnNumber>=0 && playground.getOnPosition(columnNumber,rowNumber)==playerId){
        rowNumber++
        columnNumber--
        figuresInRow++
    }
    if(figuresInRow >=4)
        return true


    return false
}