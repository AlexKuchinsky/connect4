package com.example.alex.connect4

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView

class GameFigure(radius: Float, colorId: Int, context: Context?, elevationLevel: Float = 3f, attributeSet: AttributeSet? = null): ImageView(context,attributeSet) {
    init {
        elevation = elevationLevel
        var bitmap = Bitmap.createBitmap((radius*2).toInt(),(radius*2).toInt(), Bitmap.Config.ARGB_8888)
        var canvas = Canvas(bitmap)
        val paint = Paint()
        paint.isAntiAlias = true
        paint.color = colorId
        canvas.drawBitmap(bitmap,0f,0f,paint)
        canvas.drawCircle(radius,radius,radius,paint)
        setImageBitmap(bitmap)
    }

}