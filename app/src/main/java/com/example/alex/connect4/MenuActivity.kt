package com.example.alex.connect4


import android.os.Bundle
import android.widget.Button
import android.content.Intent
import android.view.Menu
import android.view.Window
import android.view.WindowManager
import com.example.alex.connect4.GameInstructions.Playground
import com.example.alex.connect4.GameInstructions.PositionDescriptor
import com.example.alex.connect4.GameInstructions.businessLogicMakeTurn


class MenuActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Remove notification bar
        supportActionBar?.hide()
        setContentView(R.layout.activity_menu)

        val mOnePlayerButton = findViewById<Button>(R.id.one_player_button)
        val mTwoPlayersButton = findViewById<Button>(R.id.two_players_button)

        //Remove title bar


        mOnePlayerButton.setOnClickListener {
            val intent = PlaygroundActivity.getIntent(MenuActivity@this,true)
            startActivity(intent)
        }
        mTwoPlayersButton.setOnClickListener{
            val intent = PlaygroundActivity.getIntent(MenuActivity@this,false)
            startActivity(intent)
        }


    }
}


