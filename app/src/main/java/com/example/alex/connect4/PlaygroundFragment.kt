package com.example.alex.connect4

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.*
import android.media.Image
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.RelativeLayout
import java.util.*
import android.widget.Toast
import java.io.Serializable


class PlaygroundFragment: Fragment(), IPlayground {

    private lateinit var playgroundLayout: RelativeLayout
    private lateinit var playgroundImage: ImageView
    private val referee: Referee = Referee.getInstance()

    private val firstPlayerHiddenFigures: ArrayDeque<GameFigure> = ArrayDeque()
    private val secondPlayerHiddenFigures: ArrayDeque<GameFigure> = ArrayDeque()

    private val firstPlayerActiveFigures: ArrayDeque<GameFigure> = ArrayDeque()
    private val secondPlayerActiveFigures: ArrayDeque<GameFigure> = ArrayDeque()

    companion object {
    }

    override val columns: Int = 7
    override val rows: Int = 6
    private val interval: Int = 20
    private val padding: Int = 40
    private var radius: Float = 0f

    private var winnerPlayerId = 1


    private var winnerDialog: Dialog? = null

    private val playgroundLayoutWidth: Float
        get() = playgroundLayout.measuredWidth.toFloat()

    private val playgroundLayoutHeight: Float
        get() = playgroundLayout.measuredHeight.toFloat()

    private var playgroundWidth: Float = 0f
    private var playgroundHeight: Float = 0f

    private val horizontalDelta: Float
        get() = (playgroundLayoutWidth - playgroundWidth)/2f

    private val verticalDelta: Float
        get() = (playgroundLayoutHeight - playgroundHeight)/2f

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_playground,container,false)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        playgroundLayout = view.findViewById(R.id.playground_fragment_layout)

        playgroundLayout.post({
            playgroundImage = createPlayingField(columns,rows,interval,padding)
            playgroundLayout.addView(playgroundImage)
            playgroundImage.setOnTouchListener { _, event ->

                val x = event.x
                when (event.action) {
                    MotionEvent.ACTION_DOWN ->{
                        val isWinner = referee.registerTouch(mapCoordsToColumn(x))
                        if(isWinner){
                            winnerPlayerId = referee.winnerId
                            val builder = AlertDialog.Builder(this.context!!)
                            builder.setTitle(getString(R.string.congratulations_text_title))
                            builder.setMessage(winnerPlayerId.toString()+getString(R.string.congratulations_text))
                            builder.setOnDismissListener({
                                clearPlaygrounds(Pair(this,PlaygroundMatrix.getInstance()))
                            })
                            winnerDialog = builder.create()


                            winnerDialog!!.setCancelable(true)
                            winnerDialog!!.window.setGravity(Gravity.TOP)
                            winnerDialog!!.show()
                        }
                    }
                }
                true
            }


            for(i in 0..21){
                val fig1 = GameFigure(radius,resources.getColor(R.color.firstPlayerColor),context)
                fig1.visibility = View.GONE
                firstPlayerHiddenFigures.add(fig1)
                val fig2 = GameFigure(radius,resources.getColor(R.color.secondPlayerColor),context)
                fig2.visibility = View.GONE
                secondPlayerHiddenFigures.add(fig2)
                playgroundLayout.addView(fig1)
                playgroundLayout.addView(fig2)

            }
            synchronizePlaygrounds(PlaygroundMatrix.getInstance(),this)

        })
    }

    override fun updatePlayground(column: Int, row: Int, player: Int){
        if(player==1){
            val activeFigure = firstPlayerHiddenFigures.poll()
            firstPlayerActiveFigures.add(activeFigure)
            animateFigure(activeFigure,column,row)
        }
        else if(player == 2){
            val activeFigure = secondPlayerHiddenFigures.poll()
            secondPlayerActiveFigures.add(activeFigure)
            animateFigure(activeFigure,column,row)
        }
    }
    fun animateFigure(figure: GameFigure,column: Int, row: Int){
        val params : RelativeLayout.LayoutParams = RelativeLayout.LayoutParams((2*radius).toInt(),(2*radius).toInt())
        params.leftMargin = (mapColumnToCoord(column) - radius).toInt()

        figure.layoutParams = params
        figure.visibility = View.VISIBLE

        val heightAnimator = ObjectAnimator.ofFloat(figure,"y",0f, mapRowToCoord(row) - radius)
        heightAnimator.duration = 400
        heightAnimator.start()
    }

    override fun clearPlayground(){
        while (!firstPlayerActiveFigures.isEmpty()){
            val figureToHide = firstPlayerActiveFigures.poll()
            figureToHide.visibility = View.GONE
            firstPlayerHiddenFigures.add(figureToHide)
        }
        while (!secondPlayerActiveFigures.isEmpty()){
            val figureToHide = secondPlayerActiveFigures.poll()
            figureToHide.visibility = View.GONE
            secondPlayerHiddenFigures.add(figureToHide)
        }
    }

    override fun getColumnHeight(column: Int): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getOnPosition(column: Int, row: Int): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isColumnAvailable(column: Int): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun boundResult(result: Int, leftBorder: Int, rightBorder: Int): Int{
        if(result<=leftBorder)
            return 0
        if(result>=rightBorder)
            return rightBorder-1
        else
            return result
    }
    private val cellLength: Float
        get() = 2*radius+interval

    private fun mapCoordsToColumn(x: Float): Int{
        val res = (x-padding-horizontalDelta)/(radius*2+interval)
        return boundResult(res.toInt(),0,columns)
    }
    private fun mapCoordsToRow(y: Float): Int {
        val res = (y-padding)/(radius*2+interval)
        return boundResult(res.toInt(),0,rows)
    }
    private fun mapRowToCoord(row: Int): Float{
        return row*(2*radius+interval)+radius+padding
    }
    private fun mapColumnToCoord(column: Int): Float{
        return column*cellLength+radius+padding+horizontalDelta
    }




    private fun createPlayingField(columns: Int,rows: Int,interval: Int, padding: Int): ImageView
    {
        var tmpWidth = playgroundLayoutWidth
        var tmpHeight = playgroundLayoutHeight

        val tmpImageLayout = ImageView(playgroundLayout.context)
        tmpImageLayout.elevation = 6f

        if(tmpHeight < tmpWidth){
            radius = (tmpHeight - (rows-1)*interval - 2*padding)/(2*rows)
            tmpWidth = (2*padding+2*radius*columns+(columns-1)*interval)
        }
        else{
            radius = (tmpWidth - (columns-1)*interval - 2*padding)/(2*columns)
            tmpHeight = (2*padding+2*radius*rows+(rows-1)*interval)
        }

        playgroundWidth = tmpWidth
        playgroundHeight = tmpHeight

        val bitmap = Bitmap.createBitmap(tmpWidth.toInt(),tmpHeight.toInt(), Bitmap.Config.ARGB_8888)
        bitmap.eraseColor(ContextCompat.getColor(playgroundLayout.context,R.color.playgroundDefault))
        val canvas = Canvas(bitmap)
        canvas.drawBitmap(bitmap,0f,0f,Paint())

        // Paint for punching holes
        val invisiblePaint = Paint()
        invisiblePaint.isAntiAlias = true
        invisiblePaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)


        var tmpY = padding+radius
        for(i in 0..(rows-1)){
            var tmpX = padding+radius
            for(j in 0..(columns-1)){
                canvas.drawCircle(tmpX,tmpY,radius-8,invisiblePaint)
                tmpX+=2*radius+interval
            }
            tmpY += 2*radius+interval
        }
        tmpImageLayout.setImageBitmap(bitmap)

        val params : RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(playgroundLayoutWidth.toInt(),playgroundLayoutHeight.toInt())
        tmpImageLayout.layoutParams = params

        return  tmpImageLayout
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        winnerDialog?.dismiss()
    }

    // Attempt to lock touches on playgroundImageView
    /*
    class PlaygroundImageView(context: Context?): ImageView(context){

        public var isImageViewLocked: Boolean = false
        override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
            val sup = super.dispatchTouchEvent(event)
            Log.d("TAG", "\n dispatchtouchevent, isImageLocked: "+isImageViewLocked+"super: "+sup)
            return sup


        }
        override fun performClick(): Boolean {
            val sup = super.performClick()
            Log.d("TAG", "\n performClick, isImageLocked: "+isImageViewLocked+"super: "+sup)
            return sup
        }
    }
    */



}