package com.example.alex.connect4.GameInstructions

import com.example.alex.connect4.GameInstructions.Models.Chip
import com.example.alex.connect4.GameInstructions.Models.RefereeResult
import com.example.alex.connect4.GameInstructions.Models.RefereeResultEnum

class Referee {
    private fun checkWinner(playground: Playground): Chip? {
        // Checking winner
        return null
    }
    private fun checkDraw(playground: Playground): Boolean{
        // Checking draw
        return false
    }
    public fun checkState(playground: Playground): RefereeResult {
        val winnerChip = checkWinner(playground)
        if(winnerChip!=null)
            return RefereeResult(RefereeResultEnum.WIN, winnerChip)
        else if(checkDraw(playground))
            return RefereeResult(RefereeResultEnum.DRAW)
        else
            return RefereeResult(RefereeResultEnum.CONTINUE_GAME)
    }
}