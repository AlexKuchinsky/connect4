package com.example.alex.connect4.GameInstructions.Models

enum class RefereeResultEnum {
    WIN, DRAW, CONTINUE_GAME
}