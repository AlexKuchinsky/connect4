package com.example.alex.connect4
import java.util.Random


class BotPlayer(playerId: Int, val playgroundMatrix: IPlayground, val secondPlayerId: Int): Player(playerId) {
    override fun setReady(): Int {

        // FP :)
        if( tryToWin() >= 0)
            return  tryToWin()

        if(dontLetToWin() >= 0)
            return dontLetToWin()

        return goodLuck()
    }

    fun tryToWin(): Int{
        for(i in 0..playgroundMatrix.columns-1){
            if(playgroundMatrix.isColumnAvailable(i)){
                val row = playgroundMatrix.getColumnHeight(i)
                playgroundMatrix.updatePlayground(i,row,playerId)
                if(checkWinnerPosition(PlaygroundMatrix.getInstance(),i,row,playerId)){
                    playgroundMatrix.updatePlayground(i,row,0)
                    return i
                }
                playgroundMatrix.updatePlayground(i,row,0)
            }
        }
        return -1
    }
    fun dontLetToWin(): Int {
        for(i in 0..playgroundMatrix.columns-1){
            if(playgroundMatrix.isColumnAvailable(i)){
                val row = playgroundMatrix.getColumnHeight(i)
                playgroundMatrix.updatePlayground(i,row,secondPlayerId)
                if(checkWinnerPosition(PlaygroundMatrix.getInstance(),i,row,secondPlayerId)){
                    playgroundMatrix.updatePlayground(i,row,0)
                    return i
                }
                playgroundMatrix.updatePlayground(i,row,0)
            }
        }
        return -1
    }
    fun  goodLuck(): Int{
        for (i in 0..1000){
            var columnNumber = rand(0,playgroundMatrix.columns)
            if(playgroundMatrix.isColumnAvailable(columnNumber))
                return columnNumber
        }
        throw NotImplementedError()
    }





    fun rand(from: Int, to: Int) : Int {
        return Random().nextInt(to - from) + from
    }
}