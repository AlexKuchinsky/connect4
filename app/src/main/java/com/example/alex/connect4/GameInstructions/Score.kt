package com.example.alex.connect4.GameInstructions

import com.example.alex.connect4.GameInstructions.Models.Player

class Score(val players: ArrayList<Player>) {
    val playersScores:HashMap<Int, Int>
    init {
        playersScores = HashMap()
        for(player in players)
            playersScores[player.playerId] = 0
    }
    fun countPlayerWin(playerId: Int){
        playersScores[playerId]?.plus(1)
    }
    fun getPlayerScore(playerId: Int): Int{
        return playersScores[playerId]!!
    }
}