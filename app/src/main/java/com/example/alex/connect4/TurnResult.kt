package com.example.alex.connect4

enum class TurnResult {
    FAILED, SUCCESS, WINNER, DRAW
}