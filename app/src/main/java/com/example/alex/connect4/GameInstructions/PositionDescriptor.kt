package com.example.alex.connect4.GameInstructions

import com.example.alex.connect4.GameInstructions.Models.ChipPositionDescription

class PositionDescriptor {
    public fun getChipPositionDescription(xCoord: Int, width: Int, columnsCount: Int): ChipPositionDescription {
        val columnNumber = xCoord*columnsCount/width
        return ChipPositionDescription(columnNumber)
    }
}