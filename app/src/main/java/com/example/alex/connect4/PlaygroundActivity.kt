package com.example.alex.connect4

import android.content.Context
import android.content.Intent
import android.opengl.Matrix
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.ViewTreeObserver
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.alex.connect4.GameInstructions.Playground
import com.example.alex.connect4.GameInstructions.PositionDescriptor
import com.example.alex.connect4.GameInstructions.businessLogicMakeTurn
import kotlin.math.log

class PlaygroundActivity : AppCompatActivity() {


    public companion object {
        private val IS_SECOND_PLAYER_BOT = "com.example.alex.connect4.second_player_is_bot"
        fun getIntent(packageContext: Context,isSecondPlayerBot: Boolean): Intent {
            return Intent(packageContext,PlaygroundActivity::class.java).putExtra(IS_SECOND_PLAYER_BOT,isSecondPlayerBot)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playground)

        val fm = supportFragmentManager
        var fragment = fm.findFragmentById(R.id.playground_fragment_container)
        if (fragment==null){
            fragment = PlaygroundFragment()
            fm.beginTransaction().add(R.id.playground_fragment_container,fragment).commit()
        }

        val firstPlayerScore: TextView = findViewById(R.id.first_player_score)
        val secondPlayerScore: TextView = findViewById(R.id.second_player_score)

        val firstPlaerName: TextView = findViewById(R.id.first_player_name)
        val secondPlayerName: TextView = findViewById(R.id.second_player_name)

        val playgroundMatrix = PlaygroundMatrix.getInstance()

        val firstPlayer = Player(1)
        var secondPlayer = Player(2)
        if(intent.getBooleanExtra(IS_SECOND_PLAYER_BOT,false)){
            secondPlayer = BotPlayer(2,playgroundMatrix,1)
        }
        Referee.initialize(playgroundMatrix,fragment as IPlayground,
                firstPlayer,secondPlayer,
                PlayerScoreTextView(firstPlayerScore, firstPlaerName),PlayerScoreTextView(secondPlayerScore,secondPlayerName))

        val newGameButton = findViewById<Button>(R.id.new_game_button)
        val menuButton  = findViewById<Button>(R.id.menu_button)

        newGameButton.setOnClickListener{

            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.attention_text)
            builder.setMessage(R.string.new_game_attention_text)

            // add the buttons
            builder.setPositiveButton(R.string.ok_text, { _,_->
                clearPlaygrounds(Pair(PlaygroundMatrix.getInstance(),fragment))
            })
            builder.setNegativeButton(R.string.cancel_text, null)

            // create and show the alert dialog
            val dialog = builder.create()
            dialog.show()
        }
        menuButton.setOnClickListener{

            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.attention_text)
            builder.setMessage(R.string.menu_button_attention_text)

            // add the buttons
            builder.setPositiveButton(R.string.ok_text, { _,_->
                Referee.reset()
                finish()
            })
            builder.setNegativeButton(R.string.cancel_text, null)

            // create and show the alert dialog
            val dialog = builder.create()
            dialog.show()

        }

    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
    }






}

fun makeTurn(xCoord: Int, playgroundWidth: Int, playground: Playground){
    val columnsCount = playground.columns
    val chipPositionDescription = PositionDescriptor().getChipPositionDescription(xCoord, playgroundWidth, columnsCount)
    businessLogicMakeTurn(chipPositionDescription, playground)
}