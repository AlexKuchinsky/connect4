package com.example.alex.connect4

import android.content.Context
import android.graphics.Typeface
import android.widget.TextView

class PlayerScoreTextView(val scoreTextView: TextView,val nameTextView: TextView ){
    fun updateSocre(score: Int){
        scoreTextView.text = score.toString()
    }
    fun boldPlayerName(bold: Boolean){
        if(bold){
            nameTextView.setTypeface(null,Typeface.BOLD)
        }
        else{
            nameTextView.setTypeface(null, Typeface.NORMAL)
        }

    }


}