package com.example.alex.connect4

interface IPlayground  {
    val columns: Int
    val rows: Int

    fun updatePlayground(column: Int, row: Int, player: Int)
    fun clearPlayground()
    fun getColumnHeight(column: Int): Int
    fun isColumnAvailable(column: Int): Boolean
    fun getOnPosition(column: Int, row: Int ): Int
}