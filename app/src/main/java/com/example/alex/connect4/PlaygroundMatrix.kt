package com.example.alex.connect4

import java.io.Serializable
import java.util.concurrent.locks.Lock

class PlaygroundMatrix private constructor(columns: Int, rows: Int): IPlayground, Serializable {
    override val columns = columns
    override val rows = rows
    private val matrix: Array<Array<Int>> = Array<Array<Int>>(columns,{i -> Array<Int>(rows,{i -> 0})})

    // Columns and rows static initialization problem
    companion object {
        private val matrixInstance: IPlayground = PlaygroundMatrix(7,6)
        @Synchronized
        fun getInstance(): IPlayground {
            return matrixInstance
        }
    }

    public override fun getColumnHeight(column: Int): Int{
        for(i in rows-1 downTo 0){
            if(matrix[column][i]==0)
                return i
        }
        return -1
    }
    public override fun getOnPosition(column: Int, row: Int ): Int{
        return matrix[column][row]
    }

    public override fun isColumnAvailable(column: Int): Boolean{
        return getColumnHeight(column) >= 0
    }

    public override fun updatePlayground(column: Int, row: Int, player: Int){
        matrix[column][row] = player
    }

    public override fun clearPlayground() {
        for(i in 0..columns-1)
            for(j in 0..rows-1)
                matrix[i][j] = 0
    }
}