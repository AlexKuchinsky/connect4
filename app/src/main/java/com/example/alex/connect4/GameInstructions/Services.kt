package com.example.alex.connect4.GameInstructions

import com.example.alex.connect4.GameInstructions.Models.ChipPositionDescription
import com.example.alex.connect4.GameInstructions.Models.RefereeResultEnum

fun businessLogicMakeTurn(chipPositionDescription: ChipPositionDescription, playground: Playground, score: Score){
    val turnChip = playground.setChip(chipPositionDescription)
    if(turnChip!=null){
        val referee = Referee()
        val refereeResult = referee.checkState(playground)
        if(refereeResult.refereeResultEnum == RefereeResultEnum.WIN){
            score.countPlayerWin(refereeResult.chip!!.playerId)
        }
    }

}